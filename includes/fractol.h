/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 11:38:57 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/18 19:09:38 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <fcntl.h>
# include <math.h>
# include "mlx.h"
# include "ft.h"

# define RED				0x0000FF
# define GREEN				0x00FF00
# define BLUE				0xFF0000
# define WHITE				0xFFFFFF
# define BLACK				0x000000

# define WIDTH				800
# define HEIGHT				800

# define NB_F				3

typedef unsigned char		t_uchar;

typedef struct s_mlx		t_mlx;
typedef struct s_win		t_win;
typedef struct s_img		t_img;
typedef struct s_complex	t_complex;
typedef struct s_color		t_color;
typedef struct s_fractal	t_fractal;

struct						s_mlx
{
	void					*ptr;
	t_win					*win;
	t_img					*img;
	t_fractal				*f[NB_F];
	int						curr_f;
	t_complex				**consts;
	void					(*func[NB_F])(t_mlx *, t_fractal *);
};

struct						s_win
{
	void					*ptr;

	t_size					size;
};

struct						s_img
{
	void					*ptr;

	t_size					size;
	t_point					origin;

	int						shift;
	int						key_state;
	int						mouse_state;
	t_point					origin_mouse_pos;
	t_point					last_mouse_pos;

	t_uchar					*data;
	int						bpp;
	int						size_line;
	int						endian;

	int						scale;
};

struct						s_complex
{
	double					r;
	double					i;
};

struct						s_color
{
	int						value;
	t_uchar					r;
	t_uchar					g;
	t_uchar					b;
	t_uchar					a;
};

struct						s_fractal
{
	t_complex				origin;
	t_complex				end;
	int						max_i;
	char					state;
	t_point					spacing;
	t_complex				julia;
	int						radius;
};

t_mlx						*ft_mlx(int f);
t_win						*ft_win(t_mlx *mlx, char *title);
t_img						*ft_img(t_mlx *mlx);

t_complex					ft_complex(double r, double i);
t_complex					ft_op_complex(char op, t_complex c1, t_complex c2);
t_fractal					*ft_fractal(t_complex o, t_complex end, int max_i);
t_color						ft_color_rgb(int r, int g, int b, int a);
t_color						ft_color(int value);

void						ft_update(t_mlx *mlx);

char						ft_put_img(t_mlx *mlx);
char						ft_fill_img(t_img *img, t_color color);
char						ft_clear_img(t_img *img);

void						ft_draw_point(t_mlx *mlx, t_point p, t_color color);
void						ft_draw_line(t_mlx *mlx, t_point p1, t_point p2);
void						ft_draw_rect(t_mlx *mlx, t_point p1, t_point p2);
void						ft_draw_circle(t_mlx *mlx, t_point pos, int r,
								t_color c);
void						ft_color_f(t_mlx *mlx, t_point p, int i);

void						ft_revise_point(t_point *p);

void						*ft_error(char *s);
void						ft_free_all(t_mlx **mlx);

void						ft_mandelbrot(t_mlx *mlx, t_fractal *f);
void						ft_julia(t_mlx *mlx, t_fractal *f);
void						ft_circle(t_mlx *mlx, t_fractal *f);
void						ft_cantor(t_mlx *mlx, t_fractal *f);

int							ft_calcul_f(t_mlx *mlx, t_complex z, t_complex c,
								t_point p);

int							ft_key_press(int key, t_mlx *mlx);
int							ft_key_release(int key, t_mlx *mlx);
char						ft_key(t_mlx *mlx, int key, t_complex c, int f);
int							ft_mouse_down(int button, int x, int y, t_mlx *mlx);
int							ft_mouse_up(int button, int x, int y, t_mlx *mlx);
int							ft_mouse_move(int x, int y, t_mlx *mlx);
void						ft_mouse_moved(t_mlx *mlx, int x, int y,
								t_fractal *f);
int							ft_expose_hook(t_mlx *mlx);

void						ft_free_all(t_mlx **mlx);

#endif
