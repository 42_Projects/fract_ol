/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 12:21:22 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/06 17:40:19 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void		ft_error_msg(void)
{
	ft_puts_color("##################################\n", red);
	ft_puts_color("#             FRACT'OL           #\n", red);
	ft_puts_color("##################################\n\n", red);
	ft_puts_color("Options:\n'", blue);
	ft_puts_color("M", green);
	ft_puts_color("' -> Mandelbrot\n'", blue);
	ft_puts_color("J", green);
	ft_puts_color("' -> Julia\n'", blue);
	ft_puts_color("C", green);
	ft_puts_color("' -> Circles\n", blue);
}

static void		ft_msg(void)
{
	ft_puts_color("##################################\n", red);
	ft_puts_color("#             FRACT'OL           #\n", red);
	ft_puts_color("##################################\n\n", red);
	ft_puts_color("Commands:\n", blue);
	ft_puts_color("O/P", green);
	ft_puts_color("               -> ", blue);
	ft_puts_color("decrease/increase ", green);
	ft_puts_color("iterations\n", blue);
	ft_puts_color("right click/[+/-]", green);
	ft_puts_color(" -> ", blue);
	ft_puts_color("zoom in/[zoom in/out]\n", green);
	ft_puts_color("left click", green);
	ft_puts_color("        -> ", blue);
	ft_puts_color("move ", green);
	ft_puts_color("fractal\n", blue);
	ft_puts_color("U", green);
	ft_puts_color("                 -> ", blue);
	ft_puts_color("reset ", green);
	ft_puts_color("fractal\n", blue);
}

int				main(int ac, char **av)
{
	t_mlx		*mlx;

	if (ac != 2 || ft_slen(av[1]) > 1 ||
		(av[1][0] != 'M' && av[1][0] != 'J' && av[1][0] != 'C'))
	{
		ft_error_msg();
		return (0);
	}
	mlx = NULL;
	if (av[1][0] == 'M' || av[1][0] == 'J')
		mlx = ft_mlx(av[1][0] == 'J');
	else if (av[1][0] == 'C')
		mlx = ft_mlx(2);
	if (!mlx)
		return (1);
	ft_msg();
	mlx_hook(mlx->win->ptr, 2, (1L << 0), &ft_key_press, mlx);
	mlx_hook(mlx->win->ptr, 3, (1L << 1), &ft_key_release, mlx);
	mlx_hook(mlx->win->ptr, 4, (1L << 2), &ft_mouse_down, mlx);
	mlx_hook(mlx->win->ptr, 5, (1L << 3) | (1L << 5), &ft_mouse_up, mlx);
	mlx_hook(mlx->win->ptr, 6, (1L << 4) | (1L << 5), &ft_mouse_move, mlx);
	mlx_hook(mlx->win->ptr, 12, (1L << 0), &ft_expose_hook, mlx);
	ft_update(mlx);
	mlx_loop(mlx->ptr);
	return (0);
}
