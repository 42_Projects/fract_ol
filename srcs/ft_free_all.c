/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_all.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 11:51:26 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/05 12:38:19 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		ft_free_all(t_mlx **mlx)
{
	int		i;

	if (!mlx || !(*mlx)->img)
		return ;
	free((*mlx)->img->data);
	free((*mlx)->img);
	free((*mlx)->win);
	i = -1;
	while (++i < NB_F)
		free((*mlx)->consts[i]);
	free((*mlx)->consts);
	free(*mlx);
}
