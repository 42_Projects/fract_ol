/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mlx.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 13:19:06 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/18 19:09:47 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void			init_mlx(t_mlx *mlx)
{
	if (!mlx)
		return ;
	mlx->consts[0][0] = ft_complex(-2.1, -1.2);
	mlx->consts[0][1] = ft_complex(0.6, 1.2);
	mlx->consts[1][0] = ft_complex(-1, -1.2);
	mlx->consts[1][1] = ft_complex(1, 1.2);
	mlx->consts[2][0] = ft_complex(WIDTH, HEIGHT);
	mlx->consts[2][1] = ft_complex(0, 0);
	mlx->func[0] = &(ft_mandelbrot);
	mlx->func[1] = &(ft_julia);
	mlx->func[2] = &(ft_circle);
}

t_mlx			*ft_mlx(int f)
{
	t_mlx		*mlx;
	int			i;

	if (!(mlx = (t_mlx *)malloc(sizeof(t_mlx))) ||
		!(mlx->ptr = mlx_init()) ||
		!(mlx->img = ft_img(mlx)) ||
		!(mlx->win = ft_win(mlx, "Fract'ol")))
		return (ft_error("Error while creating 'mlx'.\n"));
	if (!(mlx->consts = (t_complex **)malloc(sizeof(t_complex*) * NB_F)))
		return (NULL);
	i = -1;
	while (++i < NB_F)
	{
		if (!(mlx->consts[i] = (t_complex *)malloc(sizeof(t_complex) * 2)))
			return (NULL);
	}
	mlx->curr_f = f;
	init_mlx(mlx);
	if (!(mlx->f[0] = ft_fractal(mlx->consts[0][0], mlx->consts[0][1], 500)) ||
		!(mlx->f[1] = ft_fractal(mlx->consts[1][0], mlx->consts[1][1], 500)) ||
		!(mlx->f[2] = ft_fractal(mlx->consts[2][0], mlx->consts[2][1], 50)))
		return (ft_error("Error while creating 'fractal'.\n"));
	return (mlx);
}

void			ft_update(t_mlx *mlx)
{
	if (!mlx)
		return ;
	mlx_clear_window(mlx->ptr, mlx->win->ptr);
	ft_clear_img(mlx->img);
	ft_put_img(mlx);
	mlx_put_image_to_window(mlx->ptr, mlx->win->ptr, mlx->img->ptr, 0, 0);
}
