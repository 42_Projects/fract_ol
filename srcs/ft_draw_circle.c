/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_circle.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 21:55:53 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/06 17:25:53 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		ft_draw_circle(t_mlx *mlx, t_point pos, int radius, t_color c)
{
	t_point	p;
	int		d;

	if (!mlx)
		return ;
	p = ft_point(radius, 0);
	d = 1 - radius;
	while (p.y <= p.x)
	{
		ft_draw_point(mlx, ft_point(p.x + pos.x, p.y + pos.y), c);
		ft_draw_point(mlx, ft_point(p.y + pos.x, p.x + pos.y), c);
		ft_draw_point(mlx, ft_point(-p.x + pos.x, p.y + pos.y), c);
		ft_draw_point(mlx, ft_point(-p.y + pos.x, p.x + pos.y), c);
		ft_draw_point(mlx, ft_point(-p.x + pos.x, -p.y + pos.y), c);
		ft_draw_point(mlx, ft_point(-p.y + pos.x, -p.x + pos.y), c);
		ft_draw_point(mlx, ft_point(p.x + pos.x, -p.y + pos.y), c);
		ft_draw_point(mlx, ft_point(p.y + pos.x, -p.x + pos.y), c);
		p.y++;
		if (d <= 0)
			d += 2 * p.y + 1;
		else
			d += 2 * (p.y - --p.x) + 1;
	}
}
