/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_circle.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 22:01:27 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/06 15:51:21 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void		ft_circle_draw(t_mlx *mlx, int x, int y, int radius)
{
	ft_draw_circle(mlx, ft_point(x, y), radius, ft_color(RED));
	if (radius > mlx->f[mlx->curr_f]->max_i / 10)
	{
		ft_circle_draw(mlx, x + radius / 2, y, radius / 2);
		ft_circle_draw(mlx, x - radius / 2, y, radius / 2);
		ft_circle_draw(mlx, x, y + radius / 2, radius / 2);
		ft_circle_draw(mlx, x, y - radius / 2, radius / 2);
	}
}

void			ft_circle(t_mlx *mlx, t_fractal *f)
{
	if (!mlx || !f)
		return ;
	ft_circle_draw(mlx, f->origin.r - WIDTH / 2, f->origin.i - HEIGHT / 2,
		f->radius);
}
