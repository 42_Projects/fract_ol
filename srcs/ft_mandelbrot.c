/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mandelbrot.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/22 21:41:27 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/06 17:29:58 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void			ft_mandelbrot(t_mlx *mlx, t_fractal *f)
{
	t_point		p_;
	t_complex	c;
	t_complex	z;
	t_complex	zoom;

	if ((p_.y = -1) && (!mlx || !mlx->img || !f))
		return ;
	zoom = ft_complex(WIDTH / (f->end.r - f->origin.r),
						HEIGHT / (f->end.i - f->origin.i));
	while (++p_.y < HEIGHT && (p_.x = -1))
	{
		p_.y += f->spacing.y;
		while (++p_.x < WIDTH)
		{
			p_.x += f->spacing.x;
			c.r = p_.x / zoom.r + f->origin.r;
			c.i = p_.y / zoom.i + f->origin.i;
			z = (t_complex){0, 0};
			ft_calcul_f(mlx, z, c, p_);
		}
	}
}
