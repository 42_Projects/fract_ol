/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cantor.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 22:33:24 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/06 17:13:17 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		ft_draw_cantor(t_mlx *mlx, t_point p, int len)
{
	int		i;
	int		k;

	k = mlx->f[mlx->curr_f]->max_i / 10 + 2;
	if (len >= 1 && k > 0)
	{
		ft_draw_line(mlx, p, ft_point(p.x + len, p.y));
		p.y += 20;
		ft_draw_cantor(mlx, p, len / k);
		i = 2;
		while (i < k)
		{
			ft_draw_cantor(mlx, ft_point(p.x + len * i / k, p.y), len / k);
			i += 2;
		}
	}
}

void		ft_cantor(t_mlx *mlx, t_fractal *f)
{
	if (!mlx || !f)
		return ;
	ft_draw_cantor(mlx, ft_point(f->origin.r - WIDTH + 20,
								f->origin.i - HEIGHT + 20), WIDTH - 40);
}
