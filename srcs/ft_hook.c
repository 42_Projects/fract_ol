/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hook.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 10:31:34 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/18 19:09:57 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include "keys.h"

int			ft_key_press(int key, t_mlx *mlx)
{
	t_complex	c;
	int			f;

	if (!mlx || !mlx->img)
		return (FALSE);
	if (key == 53)
	{
		ft_free_all(&mlx);
		exit(1);
	}
	if (!((f = mlx->curr_f) + 1))
		return (1);
	mlx->img->key_state = key;
	c.r = (mlx->f[f]->end.r - mlx->f[f]->origin.r) / WIDTH * 10;
	c.i = (mlx->f[f]->end.i - mlx->f[f]->origin.i) / HEIGHT * 10;
	if (ft_key(mlx, key, c, f))
		ft_update(mlx);
	return (0);
}

int			ft_expose_hook(t_mlx *mlx)
{
	if (!mlx)
		return (FALSE);
	ft_update(mlx);
	return (TRUE);
}

int			ft_mouse_move(int x, int y, t_mlx *mlx)
{
	if (!mlx || !mlx->img || !mlx->f[mlx->curr_f] ||
		(mlx->img->mouse_state == 0 && mlx->img->key_state != CTRL))
		return (0);
	ft_mouse_moved(mlx, x, y, mlx->f[mlx->curr_f]);
	return (1);
}

int			ft_mouse_down(int button, int x, int y, t_mlx *mlx)
{
	t_complex	p;
	t_complex	i;
	int			k;
	t_fractal	*f;

	if ((!mlx || !mlx->img || y <= 0 || !((f = mlx->f[mlx->curr_f]) + 1)))
		return (0);
	mlx->img->mouse_state = button;
	mlx->img->origin_mouse_pos = ft_point(x, y);
	mlx->img->last_mouse_pos = ft_point(x, y);
	if ((k = 0) || button == 4 || button == 5)
	{
		while (k < 2)
		{
			i = ft_op_complex('-', f->end, f->origin);
			p = ft_complex(i.r / 2 - i.r / WIDTH * x,
							i.i / 2 - i.i / HEIGHT * y);
			f->origin = ft_op_complex(!k ? '-' : '+', f->origin, p);
			f->end = ft_op_complex(!k ? '-' : '+', f->end, p);
			if (!k++)
				ft_key_press(button == 4 ? MORE : LESS, mlx);
		}
		ft_update(mlx);
	}
	return (button);
}

int			ft_mouse_up(int button, int x, int y, t_mlx *mlx)
{
	t_complex	p;
	t_point		p_;
	t_complex	q;
	t_complex	i;
	t_point		k;

	if (!mlx || !mlx->img || !mlx->f[mlx->curr_f])
		return (0);
	if (mlx->img->mouse_state == 2 && mlx->curr_f <= 1)
	{
		k = ft_point(x, y);
		ft_revise_point(&k);
		p_ = mlx->img->origin_mouse_pos;
		i = ft_op_complex('-', mlx->f[mlx->curr_f]->end,
								mlx->f[mlx->curr_f]->origin);
		p = ft_complex(i.r / WIDTH * p_.x, i.i / HEIGHT * p_.y);
		q = ft_complex(i.r / WIDTH * k.x, i.i / HEIGHT * k.y);
		p = ft_op_complex('+', mlx->f[mlx->curr_f]->origin, p);
		q = ft_op_complex('+', mlx->f[mlx->curr_f]->origin, q);
		mlx->f[mlx->curr_f]->origin = p;
		mlx->f[mlx->curr_f]->end = q;
		ft_update(mlx);
	}
	mlx->img->mouse_state = button > 0 ? 0 : mlx->img->mouse_state;
	return (button);
}
