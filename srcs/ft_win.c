/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_win.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 12:05:38 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/03 10:53:28 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

t_win			*ft_win(t_mlx *mlx, char *title)
{
	t_win		*win;
	t_size		size;

	title = title ? title : "Title";
	size.width = WIDTH;
	size.height = HEIGHT;
	if (!mlx || !(win = (t_win *)malloc(sizeof(t_win))) ||
		!(win->ptr = mlx_new_window(mlx->ptr, size.width, size.height, title)))
		return (ft_error("Error while creating 'win'.\n"));
	win->size = size;
	return (win);
}
