/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_line.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 20:28:13 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/06 17:26:15 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static char		draw_line(t_point *p, t_point p1, t_point p2, int *err)
{
	p->x = p2.x - p1.x < 0 ? -(p2.x - p1.x) : p2.x - p1.x;
	p->y = p2.y - p1.y < 0 ? -(p2.y - p1.y) : p2.y - p1.y;
	*err = p->x > p->y ? p->x / 2 : -p->y / 2;
	return (TRUE);
}

static void		draw_line_calculs(int *err, t_point p, t_point *p_, t_point s)
{
	int			err_;

	err_ = *err;
	*err -= err_ > -p.x ? p.y : 0;
	p_->x += err_ > -p.x ? s.x : 0;
	*err += err_ < p.y ? p.x : 0;
	p_->y += err_ < p.y ? s.y : 0;
}

void			ft_draw_line(t_mlx *mlx, t_point p1, t_point p2)
{
	t_point		p;
	t_point		p_;
	t_point		s;
	int			err;

	if (!mlx || !draw_line(&p, p1, p2, &err))
		return ;
	s = ft_point(p1.x < p2.x ? 1 : -1, p1.y < p2.y ? 1 : -1);
	p_ = ft_point(p1.x, p1.y);
	while (TRUE)
	{
		ft_draw_point(mlx, p_, ft_color(0xFF0000));
		if ((int)p_.x == (int)p2.x && (int)p_.y == (int)p2.y)
			break ;
		draw_line_calculs(&err, p, &p_, s);
	}
}

void			ft_draw_rect(t_mlx *mlx, t_point p1, t_point p2)
{
	if (!mlx)
		return ;
	ft_revise_point(&p1);
	ft_revise_point(&p2);
	ft_draw_line(mlx, p1, ft_point(p1.x, p2.y));
	ft_draw_line(mlx, p1, ft_point(p2.x, p1.y));
	ft_draw_line(mlx, ft_point(p1.x, p2.y), p2);
	ft_draw_line(mlx, ft_point(p2.x, p1.y), p2);
}
