/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_actions.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 15:25:33 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/10 17:36:13 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include "keys.h"

static char		ft_key_bis(t_mlx *mlx, int key, t_complex c, int f)
{
	if (key == CTRL)
		mlx->img->last_mouse_pos = ft_point(-1, -1);
	else if (key == U)
		mlx->f[f] = ft_fractal(mlx->consts[f][0], mlx->consts[f][1], 50);
	else if (key == TOP || key == BOT)
	{
		mlx->f[f]->origin.i += key == TOP ? c.i : -c.i;
		mlx->f[f]->end.i += key == TOP ? c.i : -c.i;
	}
	else if (key == K || key == J || key == M || key == N)
	{
		mlx->f[f]->spacing.x += key == K ? 1 : 0;
		mlx->f[f]->spacing.x -= key == J && mlx->f[f]->spacing.x > 0 ? 1 : 0;
		mlx->f[f]->spacing.y += key == M ? 1 : 0;
		mlx->f[f]->spacing.y -= key == N && mlx->f[f]->spacing.y > 0 ? 1 : 0;
	}
	else if (key == P || key == O || key == L)
	{
		mlx->f[f]->max_i += key == P ? 20 : 0;
		mlx->f[f]->max_i -= key == O && mlx->f[f]->max_i - 20 > 0 ? 20 : 0;
		mlx->f[f]->state = key == L ? !mlx->f[f]->state : mlx->f[f]->state;
	}
	else
		return (FALSE);
	return (TRUE);
}

char			ft_key(t_mlx *mlx, int key, t_complex c, int f)
{
	mlx->img->shift = key == SHIFT ? 1 : mlx->img->shift;
	if (key == MORE || key == LESS)
	{
		if (mlx->curr_f == 2)
			mlx->f[f]->radius *= key == MORE ? 1.1 : 0.9;
		else
		{
			mlx->f[f]->origin.r += key == MORE ? c.r * 5 : -c.r * 5;
			mlx->f[f]->origin.i += key == MORE ? c.i * 5 : -c.i * 5;
			mlx->f[f]->end.r -= key == MORE ? c.r * 5 : -c.r * 5;
			mlx->f[f]->end.i -= key == MORE ? c.i * 5 : -c.i * 5;
		}
	}
	else if (key == LEFT || key == RIGHT)
	{
		mlx->f[f]->origin.r += key == LEFT ? c.r : -c.r;
		mlx->f[f]->end.r += key == LEFT ? c.r : -c.r;
	}
	else if (key == TAB && mlx->img->shift)
		mlx->curr_f = (f - 1) < 0 ? NB_F - 1 : f - 1;
	else if (key == TAB)
		mlx->curr_f = (f + 1) >= NB_F ? 0 : f + 1;
	else
		return (ft_key_bis(mlx, key, c, f));
	return (TRUE);
}

static void		ft_mouse_moved_bis(t_mlx *mlx, t_point v, int f)
{
	t_complex	i;
	t_complex	p;

	i = ft_op_complex('-', mlx->f[f]->end, mlx->f[f]->origin);
	p = ft_complex(i.r / WIDTH * v.x, i.i / HEIGHT * v.y);
	mlx->f[f]->origin = ft_op_complex('-', mlx->f[f]->origin, p);
	mlx->f[f]->end = ft_op_complex('-', mlx->f[f]->end, p);
}

void			ft_mouse_moved(t_mlx *mlx, int x, int y, t_fractal *f)
{
	t_point		v;

	if (mlx->img->mouse_state == 1 ||
		(mlx->img->key_state == CTRL && mlx->curr_f == 1))
	{
		v = ft_op_point('-', ft_point(x, y), mlx->img->last_mouse_pos);
		mlx->img->last_mouse_pos = ft_op_point('+', mlx->img->last_mouse_pos,
				v);
		if (mlx->img->last_mouse_pos.x == -1 &&
				mlx->img->last_mouse_pos.y == -1)
			return ;
		if (mlx->img->key_state == CTRL && mlx->curr_f == 1)
			f->julia = ft_complex(0.001 * x + 0.2, 0.001 * y + 0.2);
		else
			ft_mouse_moved_bis(mlx, v, mlx->curr_f);
		ft_update(mlx);
	}
	else if (mlx->img->mouse_state == 2 && mlx->curr_f <= 1)
	{
		ft_update(mlx);
		ft_draw_rect(mlx, mlx->img->origin_mouse_pos, ft_point(x, y));
		mlx_put_image_to_window(mlx->ptr, mlx->win->ptr, mlx->img->ptr, 0, 0);
	}
}
