/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_img.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 12:31:55 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/10 10:40:15 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

t_img			*ft_img(t_mlx *mlx)
{
	t_img		*img;
	t_size		size;

	if (!mlx || !(img = (t_img *)malloc(sizeof(t_img))))
		return (ft_error("Error while creating 'img'.\n"));
	size = ft_size(WIDTH, HEIGHT);
	if (!(img->ptr = mlx_new_image(mlx->ptr, size.width, size.height)))
		return (ft_error("Error while creating 'img->ptr'.\n"));
	img->size = size;
	img->origin = ft_point(0, 0);
	img->data = (t_uchar *)mlx_get_data_addr(img->ptr, &img->bpp,
											&img->size_line, &img->endian);
	if (!img->data)
		return (NULL);
	img->scale = 3;
	img->shift = 0;
	img->mouse_state = 0;
	return (img);
}

char			ft_fill_img(t_img *img, t_color color)
{
	t_point		p;

	if (!img)
		return (FALSE);
	p.x = -1;
	while (++p.x < img->size.width)
	{
		p.y = -1;
		while (++p.y < img->size.height)
		{
			img->data[p.x * 4 + 0 + p.y * img->size_line] = color.r;
			img->data[p.x * 4 + 1 + p.y * img->size_line] = color.g;
			img->data[p.x * 4 + 2 + p.y * img->size_line] = color.b;
			img->data[p.x * 4 + 3 + p.y * img->size_line] = 1;
		}
	}
	return (TRUE);
}

char			ft_clear_img(t_img *img)
{
	t_point		p;

	if (!img)
		return (FALSE);
	p.x = -1;
	while (++p.x < img->size.width)
	{
		p.y = -1;
		while (++p.y < img->size.height)
		{
			img->data[p.x * 4 + 0 + p.y * img->size_line] = 0;
			img->data[p.x * 4 + 1 + p.y * img->size_line] = 0;
			img->data[p.x * 4 + 2 + p.y * img->size_line] = 0;
			img->data[p.x * 4 + 3 + p.y * img->size_line] = 0;
		}
	}
	return (TRUE);
}

char			calcul_point(t_mlx *mlx, t_point p, t_complex a, t_complex zoom)
{
	t_complex	c;
	t_complex	z;
	int			i;
	double		tmp;

	c = ft_complex(p.x / zoom.r + a.r, p.y / zoom.i + a.i);
	z = ft_complex(0, 0);
	i = 0;
	while (1)
	{
		tmp = z.r;
		z.r = z.r * z.r - z.i * z.i + c.r;
		z.i = 2 * z.i * tmp + c.i;
		if (++i == 50 || (z.r * z.r + z.i * z.i) > 4)
			break ;
	}
	if (i == 50)
		ft_draw_point(mlx, p, ft_color(BLUE));
	return (i == 50);
}

char			ft_put_img(t_mlx *mlx)
{
	(mlx->func[mlx->curr_f])(mlx, mlx->f[mlx->curr_f]);
	return (TRUE);
}
