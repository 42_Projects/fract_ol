/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_complex.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/22 20:05:33 by fbellott          #+#    #+#             */
/*   Updated: 2016/02/27 13:02:29 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

t_complex		ft_complex(double r, double i)
{
	t_complex	c;

	c.r = r;
	c.i = i;
	return (c);
}

t_complex		ft_op_complex(char op, t_complex c1, t_complex c2)
{
	if (op == '+')
		return (ft_complex(c1.r + c2.r, c1.i + c2.i));
	if (op == '-')
		return (ft_complex(c1.r - c2.r, c1.i - c2.i));
	if (op == '*')
		return (ft_complex(c1.r * c2.r, c1.i * c2.i));
	if (op == '/')
		return (ft_complex(c1.r / c2.r, c1.i / c2.i));
	return (c1);
}
