/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hook_2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 16:48:47 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/05 23:31:07 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include "keys.h"

int			ft_key_release(int key, t_mlx *mlx)
{
	if (!mlx || !mlx->img)
		return (0);
	if (key == SHIFT)
		mlx->img->shift = 0;
	mlx->img->key_state = 0;
	return (key);
}
