/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_point.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Belotte <fbellott@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 00:09:01 by Belotte           #+#    #+#             */
/*   Updated: 2016/03/10 10:38:57 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		ft_draw_point(t_mlx *mlx, t_point p, t_color color)
{
	int		pos;

	if (!mlx || !mlx->img || !mlx->win)
		return ;
	if (p.x < 0 || p.y < 0 ||
			p.x >= mlx->img->size.width || p.y >= mlx->img->size.height)
		return ;
	pos = (p.x * 4 + p.y * mlx->img->size_line);
	mlx->img->data[pos + 0] = color.b;
	mlx->img->data[pos + 1] = color.g;
	mlx->img->data[pos + 2] = color.r;
	mlx->img->data[pos + 3] = color.a;
}

void		ft_revise_point(t_point *p)
{
	p->x = p->x < 0 ? 0 : p->x;
	p->y = p->y < 0 ? 0 : p->y;
	p->x = p->x >= WIDTH ? WIDTH - 1 : p->x;
	p->y = p->y >= HEIGHT ? HEIGHT - 1 : p->y;
}
