/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractal.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/22 22:24:33 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/06 16:56:31 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

t_fractal		*ft_fractal(t_complex origin, t_complex end, int max_i)
{
	t_fractal	*f;

	if (!(f = (t_fractal *)malloc(sizeof(t_fractal))))
		return (NULL);
	f->origin = origin;
	f->end = end;
	f->max_i = max_i;
	f->state = 0;
	f->radius = 200;
	f->spacing = ft_point(0, 0);
	f->julia = ft_complex(0.285, 0.01);
	return (f);
}
