/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_julia.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/22 21:41:27 by fbellott          #+#    #+#             */
/*   Updated: 2016/03/18 19:09:24 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int				ft_calcul_f(t_mlx *mlx, t_complex z, t_complex c, t_point p)
{
	float		tmp;
	int			i;

	i = 0;
	if (!mlx || !mlx->f[mlx->curr_f])
		return (-1);
	while (TRUE)
	{
		tmp = z.r;
		z.r = z.r * z.r - z.i * z.i + c.r;
		z.i = 2 * z.i * tmp + c.i;
		if (++i == mlx->f[mlx->curr_f]->max_i || (z.r * z.r + z.i * z.i) > 2)
			break ;
	}
	ft_color_f(mlx, p, i);
	return (i);
}

void			ft_julia(t_mlx *mlx, t_fractal *f)
{
	t_point		p;
	t_complex	z;
	t_complex	zoom;

	if ((p.y = -1) && (!mlx || !mlx->img || !f))
		return ;
	zoom = ft_complex(WIDTH / (f->end.r - f->origin.r),
			HEIGHT / (f->end.i - f->origin.i));
	while (++p.y < HEIGHT && (p.x = -1))
	{
		p.y += f->spacing.y;
		while (++p.x < WIDTH)
		{
			p.x += f->spacing.x;
			z.r = p.x / zoom.r + f->origin.r;
			z.i = p.y / zoom.i + f->origin.i;
			ft_calcul_f(mlx, z, f->julia, p);
		}
	}
}

void			ft_color_f(t_mlx *mlx, t_point p_, int i)
{
	if (!mlx || !mlx->f[mlx->curr_f])
		return ;
	i += 30;
	ft_draw_point(mlx, p_, ft_color_rgb(
				i * 255 / 100,
				i / 1.5 * 255 / 100,
				i / 2.5 * 255 / 100,
				i / 1.4 * 30));
	return ;
}
