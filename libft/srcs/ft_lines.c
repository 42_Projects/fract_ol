/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lines.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 10:43:40 by fbellott          #+#    #+#             */
/*   Updated: 2016/01/27 18:38:53 by Belotte          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

t_lines		*ft_lines(char *s)
{
	t_lines	*line;

	if (!(line = (t_lines *)malloc(sizeof(t_lines))))
		return (NULL);
	line->line = ft_sdup(s);
	line->id = 0;
	line->next = NULL;
	return (line);
}
