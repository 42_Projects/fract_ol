/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssub.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbellott <fbellott@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/24 20:52:44 by fbellott          #+#    #+#             */
/*   Updated: 2016/01/24 21:26:38 by fbellott         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

char		*ft_ssub(char *s, unsigned int start, size_t len)
{
	size_t	i;
	char	*s_;

	if (!s || !(s_ = ft_snew(ft_slen(s) - (start + len) > 0 ? len :
			ft_slen(s) - start)) || len == 0 || (int)start >= ft_slen(s))
		return (NULL);
	i = -1;
	while (++i < len && s[start + i])
		s_[i] = s[start + i];
	return (s_);
}
